msgid ""
msgstr ""
"Project-Id-Version: Subscriptio Stripe\n"
"POT-Creation-Date: 2014-09-23 12:45+0300\n"
"PO-Revision-Date: 2014-09-23 12:51+0300\n"
"Last-Translator: \n"
"Language-Team: RightPress <info@rightpress.net>\n"
"Language: en_EN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-KeywordsList: __;_e;_c;__ngettext:1,2;_n:1,2;_nc:1,2;"
"__ngettext_noop:1,2;_n_noop:1,2;_x:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_ex:1,2c;"
"esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=n == 1 ? 0 : 1;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: /var/www/3.9.2/wp-content/plugins/subscriptio/"
"includes/classes/gateways/stripe\n"

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-subscriptions.class.php:58
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:165
msgid "Order"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-subscriptions.class.php:58
msgid "Subscription"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-subscriptions.class.php:64
#, php-format
msgid "Subscr %s"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-subscriptions.class.php:71
msgid "Automatic subscription payment failed (Stripe)."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-subscriptions.class.php:83
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-order.class.php:85
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:223
#, php-format
msgid "Stripe charge %s captured."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-subscriptions.class.php:89
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:229
#, php-format
msgid ""
"Stripe charge %s authorized and will be charged as soon as you start "
"processing this order. Authorization will expire in 7 days."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-account.class.php:143
msgid "Credit card deleted successfully."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-account.class.php:188
msgid "Default card changed successfully."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-order.class.php:74
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-order.class.php:80
msgid "Failed capturing previously authorized Stripe payment."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-order.class.php:126
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-order.class.php:132
msgid "Stripe refund failed."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-order.class.php:137
#, php-format
msgid "Stripe charge %s refunded."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:37
msgid "Stripe"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:89
msgid "WooCommerce Stripe payment gateway requires WooCommerce 2.0 or later."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:94
msgid ""
"WooCommerce Stripe payment gateway requires full SSL support and enforcement "
"during Checkout. Only test mode will work until this is solved."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:99
msgid ""
"WooCommerce Stripe payment gateway requires Stripe Secret Key to be set."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:104
msgid ""
"WooCommerce Stripe payment gateway requires Stripe Publishable Key to be set."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:149
msgid "Card is not selected."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:149
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:157
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:184
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:199
msgid "We have not charged your card. Please try again."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:157
msgid "Order not found."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:184
msgid "Authorization token not set."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:272
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:309
msgid "Unable to add a new card."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:347
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:354
msgid "Such card does not exist."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:383
msgid "We were not able to connect to the payment gateway. Please try again."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:388
msgid "Payment gateway error:"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:451
msgid "Response body empty."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:535
msgid "Pay Now"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:541
msgid "The card number is incorrect."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:542
msgid "The card number is not a valid credit card number."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:543
msgid "The card's expiration month is invalid."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:544
msgid "The card's expiration year is invalid."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:545
msgid "The card's security code is invalid."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:546
msgid "The card has expired."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:547
msgid "The card's security code is incorrect."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:548
msgid "The card's zip code failed validation."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:549
msgid "The card was declined."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:550
msgid "There is no card on a customer that is being charged."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:551
msgid "An error occurred while processing the card."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:552
msgid ""
"An error occurred due to requests hitting the API too quickly. Please let us "
"know if you're consistently running into this error."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:596
msgid "Enable/Disable"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:598
msgid "Enable Stripe payment gateway"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:602
msgid "Test Mode"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:604
msgid "Enable test & debug mode"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:608
msgid "Capture Immediately"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:610
msgid "Capture the charge immediately"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:611
msgid ""
"If unchecked, an authorization is issued at the time of purchase but the "
"charge itself is captured when you start processed the order. Uncaptured "
"charges expire in 7 days."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:615
msgid "Title"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:617
msgid "The title which the user sees during checkout."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:618
msgid "Credit Card - Stripe"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:621
msgid "Description"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:623
msgid "The description which the user sees during checkout."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:624
msgid "Pay Securely via Stripe"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:627
msgid "Checkout Style"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:629
msgid "Control how credit card details fields appear on your page."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:632
msgid "Inline"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:633
msgid "Modal"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:637
msgid "Checkout Image"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:639
msgid ""
"Stripe Checkout modal allows custom seller image to be displayed. Enter your "
"custom 128x128 px image URL here (should be hosted on a secure location)."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:643
msgid "Test Secret Key"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:645
msgid ""
"Test Secret Key from your Stripe Account (under Account Settings > API Keys)."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:649
msgid "Test Publishable Key"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:651
msgid "Test Publishable Key from your Stripe Account."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:655
msgid "Live Secret Key"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:657
msgid "Live Secret Key from your Stripe Account."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:661
msgid "Live Publishable Key"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:663
msgid "Live Publishable Key from your Stripe Account."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:690
msgid "Card"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:692
msgid "ending with"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:692
msgid "expires"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/includes/woocommerce-stripe-gateway.class.php:695
msgid "New Credit Card"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-form.php:29
msgid "TEST MODE"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-form.php:30
#, php-format
msgid "Click %shere%s for a list of test card numbers."
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-form.php:52
msgid "Card Number"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-form.php:53
msgid "•••• •••• •••• ••••"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-form.php:57
#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-list.php:24
msgid "Expires"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-form.php:59
msgid "Month"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-form.php:65
msgid "Year"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-form.php:73
msgid "Card Code"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-form.php:74
msgid "CVC"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-list.php:14
msgid "Linked Credit Cards"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-list.php:22
msgid "Type"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-list.php:23
msgid "Ending with"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-list.php:25
msgid "Default"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-list.php:38
msgid "Yes"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-list.php:40
msgid "Delete"
msgstr ""

#: /var/www/3.9.2/wp-content/plugins/subscriptio/includes/classes/gateways/stripe/templates/credit-card-list.php:42
msgid "Make Default"
msgstr ""
